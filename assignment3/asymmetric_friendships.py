# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 20:36:31 2016

@author: joe
"""

import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    mr.emit_intermediate(tuple(record), 1)
    mr.emit_intermediate(tuple(record[::-1]), -1)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    total = 0
    for v in list_of_values:
      total += v
#    if total == 0:
#        mr.emit((key, list_of_values))
    if total != 0:
        mr.emit(key)


# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
