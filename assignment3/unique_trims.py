# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 20:36:31 2016

@author: joe
"""

import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    mr.emit_intermediate(0, record[1][:-10])

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
#    unique = []
#    [unique.append(i) for i in list_of_values if i not in unique]
#    s = ''
#    for i in unique:
#        s += '"{0}"                 \n'.format(i)
    unique = []
    [unique.append(i) for i in list_of_values if i not in unique]
    for i in unique:
        mr.emit(i)

# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
