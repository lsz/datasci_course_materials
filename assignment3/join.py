# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 20:36:31 2016

@author: joe
"""

import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[1]
    value = record
    print('key-value',key,value)
#    words = value.split()
#    for w in words:
#        mr.emit_intermediate(w, key)
    mr.emit_intermediate(key, value)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
#    total = []
#    for v in list_of_values:
#        if v not in total:
#            total.append(v)
    orders = []
    lines = []
    for v in list_of_values:
        if v[0] == 'order':
            orders.append(v)
        if v[0] == 'line_item':
            lines.append(v)
    for o in orders:
        for l in lines:
            mr.emit(o + l)

# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
