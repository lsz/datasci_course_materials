# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 20:36:31 2016

@author: joe
"""

import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    n = 100
    m = record[0].lower()
    if m == 'a':
        i = record[1]
        for k in range(n):
            mr.emit_intermediate((i,k), record)
    if m == 'b':
        k = record[2]
        for i in range(n):
            mr.emit_intermediate((i,k), record)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    n = 100
    a = [0] * n
    b = [0] * n
    for v in list_of_values:
        if v[0].lower() == 'a':
            a[v[2]] = v[3]
#            a.append(v[3])
        if v[0].lower() == 'b':
#            b.append(v[3])
            b[v[1]] = v[3]
    total = 0
    for i in range(n):
        total += a[i] * b[i]
    if total != 0 :
        mr.emit(key + (total,))

# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
