import sys
import json

def hw():
    print 'Hello, world!'

def lines(fp):
#    print str(len(fp.readlines()))
    lines = fp.readlines()
#    metric = {}
    frequency = {}
    for json1_str in lines:
#        print(json1_str)
        if json1_str.strip()=='':
            continue
        json1_data = json.loads(json1_str.strip())
        user = None
        if 'user' in json1_data:
            user = json1_data['user']
        if user is not None:
            if 'hashtags' in user:
                hashtags=user['hashtags']
#                print('hashtags',hashtags)
                for h in hashtags:       
                    if 'text' in h:
                        w=h['text'].encode('utf-8')
#                        print(h['text'].encode('utf-8'))
                        f=frequency.get(w, 0)
                        frequency[w]=f+1
            
        entities = None
        if 'entities' in json1_data:
            entities = json1_data['entities']
        if entities is not None:
            if 'hashtags' in entities:
                hashtags=entities['hashtags']
#                print('hashtags',hashtags)
                for h in hashtags:       
                    if 'text' in h:
                        w=h['text'].encode('utf-8')
#                        print(h['text'].encode('utf-8'))
                        f=frequency.get(w, 0)
                        frequency[w]=f+1

    s=sorted(frequency, key=frequency.get, reverse=True)
    c=0
    for i in s:
        print(i+' '+str(frequency[i]))
        c+=1
        if c>=10:
            break

def main():
#    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[1])
#    hw()
    
#    #    afinnfile = open("AFINN-111.txt")
#    scores = {} # initialize an empty dictionary
#    for line in sent_file:
#        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
#        scores[term] = int(score)  # Convert the score to an integer.
##    print scores.keys() # Print every (term, score) pair in the dictionary
        
#    lines(sent_file)
    lines(tweet_file)

if __name__ == '__main__':
    main()
