# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 01:13:39 2015

@author: joe
"""

with open("problem_1_output.txt") as f:
    content = f.readlines()
    print(content[:20])
    f2 = open('output.txt','w')
    f2.writelines(content[:20]) # python will convert \n to os.linesep
    f2.close() # you can omit in most cases as the destructor will call it