import sys
import json

def hw():
    print 'Hello, world!'

def lines(fp, scores):
#    print str(len(fp.readlines()))
    lines = fp.readlines()
    metric = {}
    frequency = {}
    for json1_str in lines:
#        print(json1_str)
        if json1_str.strip()=='':
            continue
        json1_data = json.loads(json1_str.strip())
        text = json1_data['text']
        text = text.encode('utf-8')
        words = text.split()
        score=0
        for k in scores.keys():
            count=text.count(k)
            if count>0:
#                print(count)
                score+=count*scores[k]
#        print(score)
        for w in words:
            if w not in scores.keys():
                f=frequency.get(w, 0)
                m=metric.get(w, 0)
                metric[w]=(m*f+score)/(f+1)
                frequency[w]=f+1
    
    for w in metric.keys():
        print(w+' '+str(metric[w]))

def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
#    hw()
    
    #    afinnfile = open("AFINN-111.txt")
    scores = {} # initialize an empty dictionary
    for line in sent_file:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
#    print scores.keys() # Print every (term, score) pair in the dictionary
        
#    lines(sent_file)
    lines(tweet_file, scores)

if __name__ == '__main__':
    main()
