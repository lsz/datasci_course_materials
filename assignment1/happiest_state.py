import sys
import json

def hw():
    print 'Hello, world!'

def lines(fp, scores, abbr, full, shorten):
#    print str(len(fp.readlines()))
    happiness={}
    for json1_str in fp.readlines():
#        print(json1_str)
        if json1_str.strip()=='':
            continue
        json1_data = json.loads(json1_str.strip())
        text = None
        if 'text' in json1_data:
            text = json1_data['text']
        score=0
        if text is not None:
            text = text.encode('utf-8')
            for k in scores.keys():
                count=text.count(k)
                if count>0:
                    score+=count*scores[k]
                    
#        coordinates = None
#        if 'coordinates' in json1_data:
#            coordinates = json1_data['coordinates']
#        if coordinates is not None:
#            print('coordinates',type(coordinates),coordinates)
        place = None
        if 'place' in json1_data:
            place = json1_data['place']
        if place is not None:
#            print('place',type(place),place['full_name'],place['country_code'])
            if place['country_code'] == 'US':
                name=place['full_name'].split()[-1].strip()
                if name in abbr:
#                    print(name)
                    h=happiness.get(name, 0)
                    happiness[name]=h+score
        user = None
        if 'user' in json1_data:
            user = json1_data['user']
        if user is not None:
#            print('user',type(user),user['location'])
            if user['location'] is not None:
                names=user['location'].split()
                for n in names:
                    name=n.replace(',','')
                    if name in abbr:
#                        print(name)
                        h=happiness.get(name, 0)
                        happiness[name]=h+score
                    if name in full:
#                        print(name,shorten[name])
                        name=shorten[name]
                        h=happiness.get(name, 0)
                        happiness[name]=h+score

#        print(score)
#    print(happiness.items())
    items=happiness.items()
    maxn,maxh = items[0]
    for i in items:
        n,h=i
#        print(n,h)
        if h > maxh:
            maxh = h
            maxn = n
#    print(lengthen[maxn],maxh)
    print(maxn)

def main():
    statestr="""Alabama;AL
Alaska;AK
Arizona;AZ
Arkansas;AR
California;CA
Colorado;CO
Connecticut;CT
Delaware;DE
Florida;FL
Georgia;GA
Hawaii;HI
Idaho;ID
Illinois;IL
Indiana;IN
Iowa;IA
Kansas;KS
Kentucky;KY
Louisiana;LA
Maine;ME
Maryland;MD
Massachusetts;MA
Michigan;MI
Minnesota;MN
Mississippi;MS
Missouri;MO
Montana;MT
Nebraska;NE
Nevada;NV
New Hampshire;NH
New Jersey;NJ
New Mexico;NM
New York;NY
North Carolina;NC
North Dakota;ND
Ohio;OH
Oklahoma;OK
Oregon;OR
Pennsylvania;PA
Rhode Island;RI
South Carolina;SC
South Dakota;SD
Tennessee;TN
Texas;TX
Utah;UT
Vermont;VT
Virginia;VA
Washington;WA
West Virginia;WV
Wisconsin;WI
Wyoming;WY"""
    abbr=[]
    full=[]
    shorten={}
#    lengthen={}
    for l in statestr.split('\n'):
        s=l.split(';')
        full.append(s[0])
        abbr.append(s[1])
        shorten[s[0]]=s[1]
#        lengthen[s[1]]=s[0]
#    print(abbr)
#    print(full)
#    print(shorten)
    
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    
#    hw()
    
#    afinnfile = open("AFINN-111.txt")
    scores = {} # initialize an empty dictionary
    for line in sent_file:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
#    print scores.keys() # Print every (term, score) pair in the dictionary

#    lines(sent_file)
    lines(tweet_file, scores, abbr, full, shorten)
    
#    print("Philadelphia, PA".split(',')[-1].strip())
    
    sent_file.close()
    tweet_file.close()

if __name__ == '__main__':
    main()
